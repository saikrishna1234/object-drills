function invert(obj) {
  if (typeof obj === "object") {
    let newObj = {};
    for (let i in obj) {
      newObj[obj[i]] = i;
    }
    return newObj;
  } else {
    return {};
  }
}

module.exports = invert;
