function defaults(obj, defaultObj) {
  if (Array.isArray(obj)) {
    return obj;
  } else {
    let result = {};
    for (let i in obj) {
      if (obj[i] === undefined) {
        result[i] = defaultObj[i];
      } else {
        result[i] = obj[i];
      }
    }
    return result;
  }
}

module.exports = defaults;
