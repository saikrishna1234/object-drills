let getValues = require("../values.cjs");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

console.log(getValues(null));
console.log(getValues(undefined));
console.log(getValues(""));
console.log(getValues(testObject));
