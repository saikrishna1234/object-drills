const getKeys = require("../keys.cjs");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

console.log(getKeys(testObject));
console.log(getKeys(null));
console.log(getKeys(undefined));
console.log(getKeys(""));
