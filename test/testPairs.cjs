let pairs = require("../pairs.cjs");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

console.log(pairs(null));
console.log(pairs(undefined));
console.log(pairs(""));
console.log(pairs(testObject));
