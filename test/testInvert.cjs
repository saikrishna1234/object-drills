let invert = require("../invert.cjs");

let obj = { Moses: "Moe", Louis: "Larry", Jerome: "Curly" };

console.log(invert([1, 2, 3]));
console.log(invert(null));
console.log(invert("123"));
console.log(invert(undefined));
console.log(invert(obj));
