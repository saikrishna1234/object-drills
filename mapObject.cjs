function mapObject(obj, addValue) {
  let res = {};
  for (let i in obj) {
    let updated = addValue(obj[i]);
    res[i] = updated;
  }
  return res;
}

module.exports = mapObject;
