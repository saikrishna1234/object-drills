let mapObject = require("../mapObject.cjs");

let obj = { start: 5, end: 12 };

function addValue(value) {
  return value + 5;
}

console.log(mapObject(null));
console.log(mapObject(undefined));
console.log(mapObject(""));
console.log(mapObject(obj, addValue));
