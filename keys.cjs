function getKeys(obj) {
  let keysArray = [];
  for (let key in obj) {
    keysArray.push(key);
  }
  return keysArray;
}

module.exports = getKeys;
