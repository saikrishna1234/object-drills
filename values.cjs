function getValues(obj) {
  let valuesArray = [];
  for (let i in obj) {
    valuesArray.push(obj[i]);
  }
  return valuesArray;
}

module.exports = getValues;
