let defaults = require("../defaults.cjs");

let obj = {
  flavor: undefined,
  sprinkles: undefined,
  iceCream: "strawberry",
};

let defaultValues = {
  flavor: "vanilla",
  sprinkles: "lots",
  iceCream: "chocalate",
};

console.log(defaults(undefined));
console.log(defaults(null));
console.log(defaults(obj, defaultValues));
console.log(defaults([1, 2, 3]));
console.log(defaults(""));
