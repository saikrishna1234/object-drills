function getPairs(obj) {
  let pairs = [];
  for (let i in obj) {
    let k = [i, obj[i]];
    pairs.push(k);
  }
  return pairs;
}

module.exports = getPairs;
